# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### `npm i react-router-dom` ööö
To be able to use router library

### `npm i react-hook-form`
To be able to use form hook library

### `npm install @mui/material @emotion/react @emotion/styled`
To get access to Material-UI library

### `npm install @mui/icons-material`
to use all icon in material-ui library

API is fetching from Heroku and API_KEY and API_URL are sent privately to users

### 1 Login
check if the user already exists in DB, if the user does not exist, a new user is created and saved in DB

### 2 Translation 
enter words and produce character image, each translation is saved in database

### 3 Profile
The profile lists 10 latest translations


