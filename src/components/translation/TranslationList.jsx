
import getImg from "../../signLanguageImg/imageMap"
const TranslationList = ({letterState}) => {


    // Maps through the list that the user has inputted and
    // returns the translation that is going to be displayed.

    const transList = letterState.map((item, index) => {
        if (item !== " ") {
            return <p style={{display:'inline'}} key={index}><img src={getImg[item]}
                                       style={{ width: '55px', height: '55px'}}
                                       alt="Could not translate"/></p>
        }
        if (item === " ") {
            return <p style={{width: '55px'}}></p>
        }
    })
    return (

        <div>
           {transList}
        </div>
    
        
     );
}
 
export default TranslationList;