import { useForm } from "react-hook-form";
import ArrowForwardRoundedIcon from '@mui/icons-material/ArrowForwardRounded';
import './TranslationForm.css'



const TranslationForm = ({onTranslate}) => {
    const translationConfig = {
        required: true,
    }
    const {register, handleSubmit } = useForm()

    const onSubmit = ({translate}) => {
        onTranslate(translate)
     }

    return (
        <form className="form">
                <label htmlFor="translate"></label>
                <input className="input" type="text" {...register('translate', translationConfig)} placeholder="   Write word to translate!" />
                <ArrowForwardRoundedIcon onClick={handleSubmit(onSubmit)}
                 sx={{width:"45px", height: '30px', borderRadius:'15px', background:'blue', 
                 '&:hover': { cursor: "pointer", backgroundColor:"gray" }}}
                />

        </form>
     );
}
 
export default TranslationForm;