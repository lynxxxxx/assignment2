import ProfileTransItem from "./ProfileTransItem";

const ProfileTransList = ({translationList}) => {

    // Used to return the list of translation -> img.

  const trList =  translationList.map((translation, index) => 
  <ProfileTransItem key={index + '-' + translation} transItem={translation} />)

    return ( 
        <>
        <ul>
            {trList}
        </ul>
        </>
     );
}
 
export default ProfileTransList;

