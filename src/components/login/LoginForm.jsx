import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from '../../const/storageKey'
import { storageSave } from '../../utils/storage'
import {loginUser} from "../../api/user";
import './LoginForm.css'
import { Box} from "@mui/material";
import ArrowForwardRoundedIcon from '@mui/icons-material/ArrowForwardRounded';

// Used to make sure the user inputs a username that has length >= 3.

const usernameConfig = {
    required: true,
    minLength: 3
}

//  Handles the login and calls functions depending on the users input value when logging in.

const LoginForm = () => {

    const {
        register,
        handleSubmit,
        formState: {errors}
    } = useForm()

    const {user, setUser} = useUser()
    const navigate = useNavigate()

   
    const [apiError, setApiError] = useState(null)
    const [apiE, setE] = useState(false)

    // Navigates to translation page if user exists or created.

    useEffect(() => {
        //redirect to profile
        if (user !== null) {
            navigate('Translation', {replace: true})
        }
      
    }, [user, navigate])

    // Calls the api in user.js

    const onSubmit = async ({username}) => {
       const [error, userResponse] = await loginUser(username)
       if(error !==null){
        setApiError(error)
       }
       if (userResponse !== null) {
        storageSave(STORAGE_KEY_USER, userResponse)
        setUser(userResponse)
       }

    }

    return (
    
   <form className="form-login"  >
                <label htmlFor="username"></label>
                <input 
                type="text"
                className="input-login"
                placeholder="  name"
                {...register("username", usernameConfig)}
                 />
                   <ArrowForwardRoundedIcon onClick={handleSubmit(onSubmit)}
                 sx={{width:"45px", height: '30px', borderRadius:'15px', background:'blue', 
                 '&:hover': { cursor: "pointer", backgroundColor:"gray" }}}
                />
              
                { apiError && <p>{ apiError }</p> }
             
            </form>

         
     );
}
 
export default LoginForm;