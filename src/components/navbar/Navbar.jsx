import { useUser } from "../../context/UserContext";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { storageDelete } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../const/storageKey";
import "./Navbar.css";
import img from "../../signLanguageImg/Logo.png";
import "./Navbar.css";
import AccountCircleRoundedIcon from "@mui/icons-material/AccountCircleRounded";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import MenuRoundedIcon from "@mui/icons-material/MenuRounded";

/*
The main navigation for the user. Uses useeffect and boolean values
To decide what buttons or the ways for the user to navigate through the
app.
*/

const Navbar = () => {
  const { user, setUser } = useUser();
  const navigate = useNavigate();
  const [menuButton, setMenuButton] = useState(false);

  const [profileButton, setProfileButton] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClickMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  // This "window.location.href" to check what buttons to display.

  useEffect(() => {
    if (window.location.href.endsWith("/Translation")) {
      setProfileButton(true);
      return setMenuButton(false);
    }

    if (window.location.href.endsWith("/Profile")) {
      setProfileButton(false);
      return setMenuButton(true);
     }
  });

  const handleOnClickToProfile = () => {
    navigate("Profile");
    setMenuButton(true);
    setProfileButton(false);
  };

  const handleOnClickToTranslation = () => {
    navigate("Translation");
    setMenuButton(false);
    setProfileButton(true);
    setAnchorEl(null);
  };

  // When the user logs out the storage data is deleted.

  const handleLogoutClick = () => {
    if (window.confirm("Are you sure you want to logout?")) {
      storageDelete(STORAGE_KEY_USER);
      setUser(null);
      setMenuButton(false);
      setAnchorEl(null);
    }
  };

  return (
    <nav className="nav-bar">
      <div className="img-label">
        <img className="img-navbar" src={img} alt="img" />
        <h3>Lost in Translation</h3>
      </div>

      <div className="buttons">
        {user !== null && profileButton && (
          <div className="profile">
            <div className="prof-name">{user.username}</div>
            <AccountCircleRoundedIcon
              sx={{
                width: "45px",
                height: "45px",
                borderRadius: "15px",
                "&:hover": { cursor: "pointer", backgroundColor: "gray" },
              }}
              onClick={handleOnClickToProfile}
            />
          </div>
        )}
        {user !== null && menuButton && (
          <div>
            <MenuRoundedIcon
              id="demo-positioned-button"
              aria-controls={open ? "demo-positioned-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
              onClick={handleClickMenu}
            />

            <Menu
              id="demo-positioned-menu"
              aria-labelledby="demo-positioned-button"
              anchorEl={anchorEl}
              open={open}
              onClose={handleCloseMenu}
              anchorOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
            >
              <MenuItem onClick={handleLogoutClick}>Logout</MenuItem>
              <MenuItem onClick={handleOnClickToTranslation}>
                Translation
              </MenuItem>
            </Menu>
          </div>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
