import { useState } from "react";
import { translationAdd } from "../api/translation";
import TranslationForm from "../components/translation/TranslationForm";
import TranslationList from "../components/translation/TranslationList";
import { STORAGE_KEY_USER } from "../const/storageKey";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";
import './Translation.css'

const Translation = () => {
    const [letterState, setLetterState] = useState([])
    const {user, setUser} = useUser()
    let letter = []

    // Checks the input values the user has typed, displays an
    // error message if some values are not valid. Pushes the values
    // to a list to be used by other functions.

    const omTranslateWord = async (word) => {  
         if (/^[a-zA-Z\s]+$/.test(word) === false) {
          alert('Please enter words with valid characters \n (A-Z)')
         }
  
        for (let i = 0; i < word.length; i++) {
        letter.push(word[i].toLowerCase())
        setLetterState(letter)
        console.log(letter)
   }
        const [error, updateUser] = await translationAdd(user, word)
        if (error !== null) {
            return [error, null];
        }
        storageSave(STORAGE_KEY_USER, updateUser)
        setUser(updateUser)

    }
  
    return (
        <div className="translation">
            <div className="trans-form">
            <TranslationForm onTranslate={omTranslateWord} />
            </div>
            
            <div>Translation</div>
            <section className="section-sign">
            <TranslationList letterState={letterState} />

            </section>
            
        </div>
    );
}

export default withAuth(Translation);



