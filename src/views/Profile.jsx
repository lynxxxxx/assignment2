import withAuth from "../hoc/withAuth";
import { useUser } from "../context/UserContext";
import { TranslationsGet } from "../api/user";
import { TranslationsClear } from "../api/translation";
import { useEffect, useState } from "react";
import { storageSave } from "../utils/storage";
import { STORAGE_KEY_USER } from "../const/storageKey";
import ProfileTransList from "../components/profile/ProfileTransList.j";
import "./Profile.css";

// The profile page which displays the recent translations

const Profile = () => {
  const { user, setUser } = useUser();
  const [letterState, setLetterState] = useState([]);

  useEffect(() => {
    TransGet();
  }, []);

  // Gets the recent translations using the API.

  const TransGet = async () => {
    const [error, translations] = await TranslationsGet(user);
    setLetterState(translations);
  };

 // Clears all translations using the API.

  const TransClear = async () => {
    const [error, translations] = await TranslationsClear(user);
    storageSave(STORAGE_KEY_USER, translations);
    setUser(translations);
    setLetterState(translations["translations"]);
  };

  return (
    <div>
      <div className="profile-t">
        <h2>Recent translations: </h2>
        <div className='profile-section'>
        <ProfileTransList  translationList={letterState} />
        </div>
        
        <div className="f">
        <button className="button-clear" onClick={TransClear}>Clear </button>
        </div>
       
      </div>
    </div>
  );
};

export default withAuth(Profile);
