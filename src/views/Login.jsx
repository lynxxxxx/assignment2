import LoginForm from "../components/login/LoginForm";
import './Login.css'

// Login page.

const Login = () => {
    return ( 
        <div className="log-in">
        <h4 className="lab-login">Login</h4>
            <LoginForm/>
        </div>
     );
}
 
export default Login;