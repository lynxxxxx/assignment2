import {createHeaders} from "./header"
const api = process.env.REACT_APP_API_URL

//check if user does not exist 

const checkForUser = async (username) => {
    try{
        const response = await fetch (`${api}?username=${username}`)
        if (!response.ok) {
            throw new Error ("Request denied")
        }
        const data = await response.json()
        return [null, data]
    }

    catch (error) {
        return [error.message, []]
    }
}

//creates user if user does not exist
const createUser = async (username) => {
    try {
        const response = await fetch(api, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error ('Could not create username ' + username)
        }
        const data = await response.json()
        return [null, data]
    }

    catch (error) {
        return [error.message, []]
    }
}


export const loginUser = async (username) => {
    const [checkError, user] = await checkForUser(username)

    if (checkError !== null) {
        return [checkError, null]
    }

    if (user.length > 0 ) {
        return [null, user.pop()]
    }

    return await createUser(username)
}


//retrieves user then saves user translations in array and lists ten latest
export const TranslationsGet = async (user, letterState) => {
    try {
        const response = await fetch(`${api}/${user.id}`, {
            method: 'GET',
            headers: createHeaders(),

        })
        if (!response.ok) {
            throw new Error('Could not get the translations')

        }
        const result = await response.json()

        let translations = []
        for (let i = 0; i < result['translations'].length; i++) {
            translations.push(result['translations'][i])
        }

        translations.reverse()
        translations = translations.slice(0, 10)

        return [null, translations]

    } catch (error) {
        return [error.message, null]
    }
}