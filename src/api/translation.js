import { createHeaders } from "./header"


const apiUrl = process.env.REACT_APP_API_URL

// Make a PATCH method to update a part of record

export const translationAdd = async (user, translationText) => {

    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translationText]
            })
        })

        if (!response.ok){
            throw new Error('Could not update the translation')
        }

        const result = await response.json()
        return [null, result]
    }
    catch (error) {

    return[error.message, null]
    }
}

// Make a get method to get the translations from the API
export const TranslationsClear = async (user) => {

    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error('Could not clear the translations')

        }
        const result = await response.json()

        return [null, result]

    } catch (error) {
        return [error.message, null]
    }
}
